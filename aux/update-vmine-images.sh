#!/bin/sh
#
# Manage a mirror of the pre-built vmine images.
#

base_url="https://git.autistici.org/api/v4/projects/493"
target_dir="/var/lib/vmine/images"
registry_path="/etc/vmine/images.yml"
default_image="bookworm"

usage() {
    cat <<EOF
Usage: $0 [<options>]

Update locally stored VMine images by downloading them from upstream.

The tool will update the image files, and rewrite the VMine image
registry with the new paths, in such a way so as not to disturb the
ongoing operations of the vmine server.

The image registry file will be fully rewritten and any manual edits
will be lost.

Options:

  --datadir=PATH        Path for images (default "${target_dir}")
  --registry-file=PATH  Location of the image registry (default "${registry_path}")
  --default-image=NAME  Name (distro) of the default image

EOF
    exit 2
}

while [ $# -gt 0 ]; do
    case "$1" in
        --datadir=*)        target_dir="${1#*=}" ;;
        --datadir)          target_dir="$2" ; shift ;;
        --registry-file=*)  registry_path="${1#*=}" ;;
        --registry-file)    registry_path="$2" ; shift ;;
        --default-image=*)  default_image="${1#*=}" ;;
        --default-image)    default_image="$2" ; shift ;;
        --help)             usage ;;
        *) echo "Unknown argument '$1'" >&2 ; usage ;;
    esac
    shift
done

# We intend to update the configuration without it ever being invalid,
# so we don't have to coordinate with the vmine server process.
#
# So, for every image, we use a timestamped directory to build the
# image, and delete the previous one only after having successfully
# generated a new registry file.
#
# If at any point we fail, all the generated files will be deleted. To
# achieve the atomic update, we build two lists of files as we
# progress through the updates: old directories to remove, and new
# directories to keep. If at the end the status is successful, we
# remove the old dirs, otherwise we remove the new ones. To avoid
# issues with subshells and global variables, we keep the lists in
# temporary files.
old_dirs_file=$(mktemp)
exec 6> ${old_dirs_file}
emit_old_dir() {
    echo "$*" >&6
}

new_dirs_file=$(mktemp)
exec 7> ${new_dirs_file}
emit_new_dir() {
    echo "$*" >&7
}

remove_dirs_from_file() {
    for dir in $(cat "$1"); do
        echo "removing directory ${dir}"
        rm -fr "${dir}"
    done
}

cleanup() {
    local rv=$?
    if [ $rv -eq 0 ]; then
        remove_dirs_from_file ${old_dirs_file}
    else
        remove_dirs_from_file ${new_dirs_file}
    fi
    rm -f ${old_dirs_file}
    rm -f ${new_dirs_file}
    rm -f ${tmp_registry}
}

cur_timestamp=$(date +%s)

# Accumulate generated files in tmp_dirs.
tmp_registry=$(mktemp)
trap cleanup EXIT TERM INT

emit_image() {
    local image_name="$1"
    local image_path="$(realpath "$2")"
    local kernel_path="$(echo ${image_path}/vmlinuz*)"
    local initrd_path="$(echo ${image_path}/initrd*)"
    cat >>${tmp_registry} <<EOF
  ${image_name}:
    path: "${image_path}/rootfs.img"
    kernel_path: "${kernel_path}"
    initrd_path: "${initrd_path}"
EOF
}

atomic_registry_update() {
    echo "---" > ${tmp_registry}
    echo "default: ${default_image}" >> ${tmp_registry}
    echo "images:" >> ${tmp_registry}

    "$@" || exit 1

    mv -f ${tmp_registry} ${registry_path} \
        || exit 1
}

download_image() {
    local pkg_name="$1"
    local pkg_version="$2"
    local pkg_id="$3"
    local target="$4"

    # Find the .tar filename in the files list.
    filename=$(curl -s "${base_url}/packages/${pkg_id}/package_files" \
                   | jq -r '.[] | .file_name' \
                   | grep '\.tar$' \
                   | head -1)
    if [ -z "$filename" ]; then
        echo "ERROR: unable to find filename for package ID ${pkg_id}" >&2
        exit 1
    fi

    local url="${base_url}/packages/generic/${pkg_name}/${pkg_version}/${filename}"

    echo "downloading and unpacking ${url}"
    curl -s "${url}" \
        | tar -C "${target}" -x -f - \
        || exit 1
}

install_image() {
    local pkg_name="$1"
    local pkg_version="$2"
    local pkg_id="$3"

    local base_img_dir="${target_dir}/${pkg_name}"
    find ${base_img_dir} -mindepth 1 -maxdepth 1 -type d 2>/dev/null \
         | while read dir ; do emit_old_dir ${dir} ; done

    local target="${base_img_dir}/${cur_timestamp}"
    mkdir -p ${target}
    emit_new_dir ${target}

    download_image ${pkg_name} ${pkg_version} ${pkg_id} ${target}
    emit_image ${pkg_name} ${target}
}

update_images() {
    # Find the images that are distributed by the upstream by querying
    # the Gitlab Packages API. This obscure black magic one-liner
    # gives us the most recent image for a given Debian distribution.
    curl -sf "${base_url}/packages" \
        | jq -r '.[] | [.name, .version, .id] | @tsv' \
        | sort -k1,1 -k2,2Vr \
        | awk '!seen[$1]++' \
        | while read pkg_name pkg_version pkg_id; \
        do install_image ${pkg_name} ${pkg_version} ${pkg_id}; done
}

atomic_registry_update update_images

# Reload the vmine service.
systemctl reload vmine.service

exit 0
