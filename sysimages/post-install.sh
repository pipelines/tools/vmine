#!/bin/sh

TARGET="$1"
if [ -z "$TARGET" ]; then
    echo "Usage: $0 TARGET" >&2
    exit 2
fi

set -e
set -x

umask 022
export DEBIAN_FRONTEND=noninteractive

# Make dpkg faster by skipping fsync().
echo force-unsafe-io > $TARGET/etc/dpkg/dpkg.cfg.d/no-sync

# Compress downloaded indexes to take up less disk space.
echo 'Acquire::GzipIndexes "true";' > $TARGET/etc/apt/apt.conf.d/90gzip-indexes

# Skip download of 'Translations' files.
echo 'Acquire::Languages "none";' > $TARGET/etc/apt/apt.conf.d/90acquire-no-languages

# Avoid failures due to transient timeouts.
echo 'Acquire::Retries "3";' > $TARGET/etc/apt/apt.conf.d/90retry

# Avoid cron interference with apt-get update.
echo 'APT::Periodic::Enable "0";' > $TARGET/etc/apt/apt.conf.d/02periodic

# Install required packages. We need an SSH server and a Python
# installation for Ansible.
PACKAGES="openssh-server ca-certificates python3-minimal python3-apt libpam-systemd gpg"

if [ ! -e $TARGET/usr/sbin/policy-rc.d ]; then
    echo 'exit 101' > $TARGET/usr/sbin/policy-rc.d
    chmod +x $TARGET/usr/sbin/policy-rc.d
fi

chroot $TARGET apt-get install -y --no-install-recommends $PACKAGES

# Preconfigure 'grub', to avoid it breaking on test VMs every time the
# package is upgraded post-release. We're just going to set a reasonable
# default for the installation disk, that works with vmine (/dev/vda).
cat <<EOF | chroot $TARGET debconf-set-selections
grub-pc grub-pc/install_devices multiselect /dev/vda
grub-pc grub-pc/install_devices_empty boolean false
EOF

# Fix up sources.list with -updates and -security, in a distro-independent way.
sources_list_distro=$(awk '/^deb / {print $3}' $TARGET/etc/apt/sources.list | head -1)
sources_list_components=$(grep '^deb ' $TARGET/etc/apt/sources.list | head -1 | sed -e 's,^.*main,main,')
echo "deb http://deb.debian.org/debian ${sources_list_distro}-updates ${sources_list_components}" \
     >> $TARGET/etc/apt/sources.list
echo "deb http://deb.debian.org/debian-security ${sources_list_distro}-security ${sources_list_components}" \
     >> $TARGET/etc/apt/sources.list

# Now run apt upgrade to catch up with the latest upgrades.
chroot $TARGET apt-get update
chroot $TARGET apt-get -y upgrade

# Install the network configuration script.
cat > $TARGET/usr/sbin/vmine-network-config <<EOF
#!/bin/sh

DEVICE="\${1:-eth0}"

for arg in \$(cat /proc/cmdline); do
  case "\$arg" in
    vmine_ip=*)
      ip=\$(echo "\${arg#*=}" | cut -d: -f1)
      gw=\$(echo "\${arg#*=}" | cut -d: -f2)
      ip addr add "\$ip" dev \$DEVICE
      ip route add default via "\$gw"
      ;;
    vmine_dns=*)
      resolver="\${arg#*=}"
      echo "nameserver \${resolver}" > /etc/resolv.conf
      ;;
    vmine_hostname=*)
      hostname="\${arg#*=}"
      echo "\${hostname}" > /etc/hostname
      hostname "\${hostname}"
      echo "127.0.0.1 \${hostname}" >> /etc/hosts
      ;;
    vmine_ssh_key=*)
      ssh_key="\${arg#*=}"
      mkdir -p /root/.ssh
      chmod 0700 /root/.ssh
      echo "\${ssh_key}" | tr _ ' ' > /root/.ssh/authorized_keys
      ;;
  esac
done

EOF

chmod 755 $TARGET/usr/sbin/vmine-network-config

# Configure ifupdown to start our script for eth0.
cat > $TARGET/etc/network/interfaces.d/eth0 <<EOF
auto eth0
iface eth0 inet manual
    up /usr/sbin/vmine-network-config eth0
EOF

# Set zstd compression for the initramfs (if not default already).
if ! grep -q ^COMPRESS=zstd $TARGET/etc/initramfs-tools/initramfs.conf; then
    echo COMPRESS=zstd > $TARGET/etc/initramfs-tools/conf.d/compress
    chroot $TARGET update-initramfs -u
fi

# Clear package cache.
chroot $TARGET apt-get clean

rm -f $TARGET/usr/sbin/policy-rc.d
rm -f $TARGET/var/cache/apt/lists/*

exit 0
