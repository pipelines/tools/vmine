#!/bin/sh

set -eu

if [ "$#" -lt 2 ]; then
    echo "$0 usage: IMAGE OUTPATH"
    echo "   Extract kernel/initrd from IMAGE into OUTPATH"
    exit 1
fi

image=$1
if [ ! -e "$image" ]; then
    echo "ERROR: image file '$image' does not exist" >&2
    exit 1
fi

outpath=$2
if [ ! -d "$outpath" ]; then
    echo "ERROR: output path '$outpath' does not exist or is not a directory" >&2
    exit 1
fi

mntpoint=

cleanup() {
    set +e
    if [ -n "$mntpoint" ]; then
        umount $mntpoint
        rmdir $mntpoint
    fi
    kpartx -d $image
}
trap cleanup INT TERM EXIT

dev=$(kpartx -av $image | awk '/^add map/ {print "/dev/mapper/" $3}' | head -1)
if [ -z "$dev" ]; then
    kpartx -av $image
    echo "ERROR: unable to mount $image" >&2
    exit 1
fi

mntpoint=$(mktemp -d)

echo "Mounting $image on $dev" >&2
mount -r $dev $mntpoint

for i in vmlinuz initrd.img; do
    cp -v ${mntpoint}/$(readlink ${mntpoint}/$i) $outpath
done

if [ -e ${mntpoint}/etc/debian_version ]; then
    cp -v ${mntpoint}/etc/debian_version $outpath
fi

exit 0
