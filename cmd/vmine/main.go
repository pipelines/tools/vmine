package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"git.autistici.org/pipelines/tools/vmine"
	yaml "gopkg.in/yaml.v3"
)

var (
	addr             = flag.String("addr", ":4949", "listen address for the HTTP API")
	imagesConfigFile = flag.String("images", "images.yml", "image registry")
	memoryLimit      = flag.Int("memory-limit", 0, "do not allocate more than `N` MB")
)

type imageRegistryConfig struct {
	Images       map[string]*vmine.Image `yaml:"images"`
	DefaultImage string                  `yaml:"default"`
}

// Initialize the image registry.
func initImageRegistry() (*imageRegistryConfig, error) {
	f, err := os.Open(*imagesConfigFile)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var c imageRegistryConfig
	if err := yaml.NewDecoder(f).Decode(&c); err != nil {
		return nil, fmt.Errorf("error parsing image registry file: %w", err)
	}
	if len(c.Images) == 0 {
		// This is just a warning, to allow the Debian package
		// to start an unconfigured server.
		log.Printf("warning: no images are defined in the image registry!")
	} else {
		if c.DefaultImage == "" {
			return nil, errors.New("no default image specified in the registry")
		}
		if _, ok := c.Images[c.DefaultImage]; !ok {
			return nil, fmt.Errorf("default image '%s' does not appear in the registry", c.DefaultImage)
		}
	}

	return &c, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	rc, err := initImageRegistry()
	if err != nil {
		log.Fatalf("error loading configuration: %v", err)
	}
	vmine.LoadImageRegistry(rc.Images, rc.DefaultImage)

	engine := vmine.NewEngine(*memoryLimit)

	// Install a signal handler for configuration reload.
	hupCh := make(chan os.Signal, 1)
	go func() {
		for range hupCh {
			log.Printf("received SIGHUP, reloading configuration")
			rc, err = initImageRegistry()
			if err != nil {
				log.Printf("error loading configuration: %v", err)
			} else {
				vmine.LoadImageRegistry(rc.Images, rc.DefaultImage)
			}
		}
	}()
	signal.Notify(hupCh, syscall.SIGHUP)

	// Install a signal handler to nicely terminate the engine.
	termCh := make(chan os.Signal, 1)
	go func() {
		<-termCh
		log.Printf("terminating due to signal")
		engine.Stop()
	}()
	signal.Notify(termCh, syscall.SIGTERM, syscall.SIGINT)

	// Spawn the HTTP server on a goroutine.
	go http.ListenAndServe(*addr, vmine.NewHTTPEngine(engine))

	engine.Wait()
}
