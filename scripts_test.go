package vmine

import "testing"

type testScriptFunc func() SetupTask

func TestScripts_Templates(t *testing.T) {
	// Verify that the built-in templates do not contain errors,
	// causing renderTemplate to panic at runtime. The actual
	// values used don't matter.
	testFuncs := []testScriptFunc{
		func() SetupTask {
			return attachInterfaceToBridge("br1", "tap1")
		},
		func() SetupTask {
			return allowForwarding("dev")
		},
		func() SetupTask {
			return masquerade("out1", "10.0.0.0/8")
		},
		func() SetupTask {
			return createBridgeInterface("br1", "10.0.0.1/24")
		},
		func() SetupTask {
			return createTapInterface("tap1")
		},
	}

	for _, f := range testFuncs {
		// The nil check is mostly to fool the compiler, we
		// don't care about the result.
		if res := f(); res == nil {
			t.Errorf("nil result")
		}
	}
}
